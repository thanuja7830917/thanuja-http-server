const http = require('http')

const path = require('path')

const fs = require('fs')

const uuid = require('uuid')

const server = http.createServer((req, res) => {
    try {

        const filepath = path.join(__dirname, 'public', req.url == '/html' ? 'content.html' : req.url)

        const extname = path.extname(filepath)

        let contentType = ""

        switch (extname) {
            case '.json':
                contentType = 'application/json'
                break
            case '.html':
                contentType = 'text/html'
                break
            case '.css':
                contentType = 'text/css'
                break
            case '.js':
                contentType = 'text/javascript'
                break
        }
        
        if (req.url == '/html') {
            fs.readFile(path.join(__dirname, 'public', 'content.html'), (err, data) => {
                if (err) {
                    throw err
                }
                else {
                    res.writeHead(200, { 'Content-type': 'text/html' })
                    res.end(data)
                }
            })
        }

        if (req.url == '/json') {
            fs.readFile(path.join(__dirname, 'public', 'content.json'), 'utf-8', (err, data) => {
                if (err) {
                    throw err
                }
                else {
                    res.writeHead(200, { 'Content-type': contentType })
                    res.end(data)
                }
            })
        }

        if (req.url == '/uuid') {

            res.writeHead(200, { 'Content-type': 'text' })

            res.end(JSON.stringify({ 'id': uuid.v4() }))
        }

        if (req.url.includes('/status')) {
            const status = req.url.split('/')
            res.writeHead(200, { 'Content-type': 'text' })
            res.end(status[status.length - 1])
        }

        if (req.url.includes('/delay')) {
            const limit = req.url.split('/')
            setTimeout(() => {
                res.writeHead(200, { 'Content-type': "text" })
                res.end('hii')
            }, Number(limit[limit.length - 1]) * 1000)
        }
    }
    catch (err) {
        console.error(err)
    }
}).listen(8000, () => { console.log("server listening on port 8000") })